package com.project.sasa.core;

import com.mysql.jdbc.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Iterator;
import java.util.Vector;

public class Main {

    private static final int PRODUCT_CODE_COL = 0;
    private static final int PRODUCT_NAME_COL = 1;
    private static final int TRADEMARK_COL = 2;
    private static final int DETAIL_COL = 3;
    private static final int DESCRIPTION_COL = 4;
    private static final int MAIN_CATEGORY_COL = 5;
    private static final int CATEGORY_COL = 6;
    private static final int SUB_CATEGORY_COL = 7;
    private static final int COLORTYPE_COL = 8;
    private static final int MODEL_CODE_COL = 9;
    private static final int PRICE_COL = 10;

    public static void main(String[] args) {
        try {

            FileInputStream fis = new FileInputStream("./Product-Document.xlsx");

            XSSFWorkbook workbook = (XSSFWorkbook) WorkbookFactory.create(fis);
            Sheet sheet1 = workbook.getSheetAt(0);
            Iterator rowIterator = sheet1.rowIterator();
            Vector<Vector<Cell>> rowVector = new Vector<Vector<Cell>>();

            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "");
            con.setAutoCommit(false);

            rowIterator.next();
            while (rowIterator.hasNext()) {

                long tmId, mcId, catId, scId;
                XSSFRow row = (XSSFRow) rowIterator.next();
                Cell tmCell = row.getCell(TRADEMARK_COL);
                String trademarkQuery = "select * from trademark where name='" + tmCell.getStringCellValue() + "';";
                PreparedStatement  st = con.prepareStatement(trademarkQuery);
                ResultSet trademarkRs = st.executeQuery(trademarkQuery);
                if (!trademarkRs.next()) {
                    throw new Exception("No such trademark as " + tmCell.getStringCellValue());
                } else {
                    tmId = trademarkRs.getLong("id");
                }

                Cell mcCell = row.getCell(MAIN_CATEGORY_COL);
                String mcQuery = "select * from main_category where name='" + mcCell.getStringCellValue() + "';";
                PreparedStatement mcSt = con.prepareStatement(mcQuery);
                ResultSet mcRs = mcSt.executeQuery(mcQuery);
                if (!mcRs.next()) {
                    throw new Exception("No such main category as " + mcCell.getStringCellValue());
                } else {
                    mcId = mcRs.getLong("id");
                }

                Cell catCell = row.getCell(CATEGORY_COL);
                String catQuery = "select * from category where name='" + catCell.getStringCellValue() + "' and owner="+ mcId + ";";
                PreparedStatement  catSt = con.prepareStatement(catQuery);
                ResultSet catRs = catSt.executeQuery(catQuery);
                if (!catRs.next()) {
                    throw new Exception("No such category as " + catCell.getStringCellValue());
                } else {
                    catId = catRs.getLong("id");
                }

                Cell scCell = row.getCell(SUB_CATEGORY_COL);
                String scQuery = "select * from sub_category where name='" + scCell.getStringCellValue() + "' and owner="+ catId + ";";
                PreparedStatement  scSt = con.prepareStatement(scQuery);
                ResultSet scRs = scSt.executeQuery(scQuery);
                if (!scRs.next()) {
                    throw new Exception("No such subcategory as " + scCell.getStringCellValue());
                } else {
                    scId = scRs.getLong("id");
                }

                String insertProductQuery = "INSERT INTO product (version, code, color_type, description, name, price, category, main_category, sub_category, trademark) VALUES(?,?,?,?,?,?,?,?,?,?)";

                PreparedStatement insertProductSt = con.prepareStatement(insertProductQuery, PreparedStatement.RETURN_GENERATED_KEYS);
                insertProductSt.setInt(1, 0);
                insertProductSt.setString(2, row.getCell(PRODUCT_CODE_COL).getStringCellValue());
                insertProductSt.setString(3, row.getCell(COLORTYPE_COL).getStringCellValue());
                if (row.getCell(DESCRIPTION_COL) != null && !StringUtils.isNullOrEmpty(row.getCell(DESCRIPTION_COL).getStringCellValue())) {
                    insertProductSt.setString(4, row.getCell(DESCRIPTION_COL).getStringCellValue());
                } else {
                    insertProductSt.setString(4, null);
                }
                insertProductSt.setString(5, row.getCell(PRODUCT_NAME_COL).getStringCellValue());
                insertProductSt.setDouble(6, row.getCell(PRICE_COL).getNumericCellValue());
                insertProductSt.setLong(7, catId);
                insertProductSt.setLong(8, mcId);
                insertProductSt.setLong(9, scId);
                insertProductSt.setLong(10, tmId);

                insertProductSt.executeUpdate();

                ResultSet resultSet = insertProductSt.getGeneratedKeys();

                int insertedProductId;
                if(resultSet.next()) {
                    insertedProductId = resultSet.getInt(1);
                } else {
                    throw new Exception("Unsuccessful insert at row " + row.getRowNum());
                }

                System.out.println(row.getRowNum());

                if (row.getCell(DETAIL_COL) != null && !StringUtils.isNullOrEmpty(row.getCell(DETAIL_COL).getStringCellValue())) {
                    String details = row.getCell(DETAIL_COL).getStringCellValue();
                    String[] parsedDetails = details.split("/");

                    for (String parsedDetail : parsedDetails) {
                        String[] keyValue = parsedDetail.split(":");

                        String insertDetailQuery = "INSERT INTO product_detail (version, detail_key, detail_value, owner) VALUES(?,?,?,?)";

                        PreparedStatement insertDetailSt = con.prepareStatement(insertDetailQuery);
                        insertDetailSt.setInt(1, 0);
                        insertDetailSt.setString(2, keyValue[0].trim());
                        insertDetailSt.setString(3, keyValue[1].trim());
                        insertDetailSt.setLong(4, insertedProductId);

                        insertDetailSt.executeUpdate();
                    }
                }


                String specs = row.getCell(MODEL_CODE_COL).getStringCellValue();

                String[] parsedSpecs = specs.split("/");

                for (String parsedSpec : parsedSpecs) {

                    String[] codeSizeQuantity = parsedSpec.split(":");

                    String sizeQuery = "select * from product_size where name='" + codeSizeQuantity[1].trim() + "';";
                    PreparedStatement sizeSt = con.prepareStatement(sizeQuery);
                    ResultSet rs = sizeSt.executeQuery(sizeQuery);

                    long sizeId;
                    if (!rs.next()) {
                        throw new Exception("No such size as " + codeSizeQuantity[1].trim());
                    } else {
                        sizeId = rs.getLong("id");
                    }

                    String insertSpecQuery = "INSERT INTO product_spec (version, model_code, quantity, owner_product, product_size) VALUES(?,?,?,?,?)";

                    PreparedStatement insertSpecSt = con.prepareStatement(insertSpecQuery);
                    insertSpecSt.setInt(1, 0);
                    insertSpecSt.setString(2, codeSizeQuantity[0].trim());
                    insertSpecSt.setInt(3, Integer.parseInt(codeSizeQuantity[2].trim()));
                    insertSpecSt.setLong(4, insertedProductId);
                    insertSpecSt.setLong(5, sizeId);

                    insertSpecSt.executeUpdate();
                }

            }

            con.commit();
            con.close();
            fis.close();
            System.out.println("Success import excel to mysql table");

        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }
}
